import Config from '../config';
//import querystring from 'querystring';
import React from "react";
import WordCloud from 'wordcloud';

interface FetchOptions extends RequestInit {
}

const getWordList = (): Promise<WordList> => {
    const path = '/first-word-count.json';

    const options: FetchOptions = {
        method: 'GET',
    };

    return fetch(`${Config.HOST_URL}${path}`, options)
        .then((response) => response.json())
        .then((wordList) => wordList);
};

type WordList = any[];

interface WordCloudState {
    wordList: WordList;
}

class WordCloudDiv extends React.Component<{}, WordCloudState> {
    public state = { wordList: [] }
    public componentDidMount(): void {
        this.getDiscussionList().then((wordList) => {
            return Object.keys(wordList).map((word: string) => {
                return [
                    word,
                    // @ts-ignore
                    wordList[word]
                ];
            });
        }).then((list) => {
            // @ts-ignore
            WordCloud(document.querySelector('#myCanvas'), {
                list,
                minSize: 8,
                // gridSize: 10,
                // shape: 'square',
                weightFactor: 4,
                // @ts-ignore
                gridSize: Math.round(10 * document.querySelector('#myCanvas').width / 800),
            });
            return list;
        }).then((wordList) => this.setState({
            wordList,
        }));
    }

    public getDiscussionList(){
        return getWordList();
    }
    public render(){
        return (
            <div className="WordCloud">
                <canvas id="myCanvas" width={1000} height={1000}/>
            </div>
        );
    };
}

export default WordCloudDiv;