import React from 'react';
import './App.css';
import WordCloudDiv from "./components/WordCloudDiv";

const App: React.FC = () => {
  return (
    <WordCloudDiv />
  );
}

export default App;
